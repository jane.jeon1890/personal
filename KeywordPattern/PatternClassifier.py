class PatternClassifier:

    def __init__(self):

        from ReadCsv import ReadCsv
        ReadCsv.__init__(self)

        from Toolbox import Toolbox
        self.tool = Toolbox()


    def loop(self):
        keyword = self.keyword
        syscategory = self.syscategory
        i = 0

        while i < len(keyword) - 1:
            self.tool.isTvProgram(keyword[i], syscategory[i])
            self.tool.isMoviePremiere(keyword[i], syscategory[i])
            self.tool.isTheater(keyword[i], syscategory[i])
            self.tool.isBusNumber(keyword[i], syscategory[i])
            self.tool.isBusStopNumber(keyword[i], syscategory[i])
            self.tool.isStockSymbol(keyword[i], syscategory[i])
            self.tool.isShippingNumber(keyword[i], syscategory[i])
            self.tool.isPhoneNumber(keyword[i], syscategory[i])
            self.tool.isAreaCode(keyword[i], syscategory[i])
            self.tool.isBus(keyword[i], syscategory[i])
            self.tool.isSubwayLine(keyword[i], syscategory[i])
            self.tool.isTVGuide(keyword[i], syscategory[i])
            self.tool.isCellPhone(keyword[i], syscategory[i])
            #self.tool.isDate(keyword[i], syscategory[i])
            self.tool.isCurrency(keyword[i], syscategory[i])
            i = i + 1

        self.tool.PrintResult(syscategory)
        # self.tool.exportCsv()




