import re
import pandas as pd

class Toolbox:

    def __init__(self):

        self.result = []

        self.programtitle = input("프로그램 제목을 입력하세요: ")
        self.tvgenere = input("반복회차 프로그램 장르를 입력하세요: ")

        self.tvguidegenere = input("TV 편성표 장르를 입력하세요: ")


    def isTvProgram(self, text, syscategory):
        pattern = "{프로그램명} + {회|차|화}"
        if re.search(self.programtitle, text) and re.search('\d(회|화|차)', text):
            self.result.append([text, syscategory, "방송 > 프로그램/채널 > " + self.tvgenere, pattern])
            return True
        else:
            return False

    def isMoviePremiere(self, text, syscategory):
        pattern = "{개봉|개봉예정|상영일}"
        if re.search('(개봉|개봉예정|상영일)', text):
            self.result.append([text, syscategory, "영화 > 영화정보", pattern])
            return True
        else:
            return False

    def isTheater(self, text, syscategory):
        pattern = "{cgv|메가박스|롯데시네마}"
        if re.search('(cgv|CGV|메가박스|롯데시네마)', text) and re.search('^((?!채널).)*$', text) and re.search('^((?!편성표).)*$', text):
            self.result.append([text, syscategory, "영화 > 영화관", pattern])
            return True
        else:
            return False
    
    def isBusNumber(self, text, syscategory):
        pattern = "{4자리 이하 숫자}"
        if re.compile(r'\d{1,4}$').match(text):
            self.result.append([text, syscategory, "지도/지역/교통 > 교통수단 > 버스", pattern])
            return True
        else:
            return False
    
    def isBusStopNumber(self, text, syscategory):
        pattern = "{5자리 숫자}"
        if re.compile(r'\d{5,5}$').match(text):
            self.result.append([text, syscategory, "지도/지역/교통 > 도로 > 도로/노선", pattern])
            return True
        else:
            return False
    
    def isStockSymbol(self, text, syscategory):
        pattern = "{6자리 숫자}"
        if re.compile(r'\d{6,6}$').match(text):
            self.result.append([text, syscategory, "정치/경제/금융 > 증권/기업 > 주식시세정보", pattern])
            return True
        else:
            return False
    
    def isShippingNumber(self, text, syscategory):
        pattern = "{10자리 이상, 13자리 이하}"
        if re.compile(r'\d{10,13}$').match(text):
            self.result.append([text, syscategory, "도구 > 조회기 > 택배조회", pattern])
            return True
        else:
            return False
    
    def isPhoneNumber(self, text, syscategory):
        pattern = "{000-0000-0000 | 0000-0000 | 00-000-0000 | 00-0000-0000}"
        if re.search('(\d{3}-\d{4}-\d{4}|\d{4}-\d{4}|\d{2}-\d{3}-\d{4}|\d{2}-\d{4}-\d{4})', text):
            self.result.append([text, syscategory, "도구 > 조회기 > 지역정보", pattern])
            return True
        else:
            return False
    
    def isAreaCode(self, text, syscategory):
        pattern = "{지역|지역번호}+{숫자}"
        if re.search('지역번호', text) or re.search('지역', text) and re.search('\d', text):
            self.result.append([text, syscategory, "도구 > 조회기 > 지역정보", pattern])
            return True
        else:
            return False
    
    def isBus(self, text, syscategory):
        pattern = "{버스}+{숫자}"
        if re.search('버스', text) and re.search('\d', text):
            self.result.append([text, syscategory, "지도/지역/교통 > 교통수단 > 버스", pattern])
            return True
        else:
            return False
    
    def isSubwayLine(self, text, syscategory):
        pattern = "{숫자}+{호선}"
        if re.search('\d(호선)', text):
            self.result.append([text, syscategory, "지도/지역/교통 > 교통수단 > 지하철", pattern])
            return True
        else:
            return False
    
    def isTVGuide(self, text, syscategory):
        pattern = "{프로그램명}+{편성표}"
        if re.search(self.tvguidegenere, text) and re.search('편성표', text):
            self.result.append([text, syscategory, "방송 > 프로그램/채널 >" + self.tvguidegenere, pattern])
            return True
        else:
            return False
    
    def isCellPhone(self, text, syscategory):
        pattern = "{아이폰|아이패드}+{모델명}"
        if re.search('(아이폰|아이패드)\w', text):
            self.result.append([text, syscategory, "쇼핑 > 디지털/가전 > 휴대폰", pattern])
            return True
        else:
            return False
    
    # def isDate(self, text, syscategory):
    #     if re.search('\d(년|월|일)', text):
    #         self.result.append([text, syscategory, "도구 > 조회기 > 날짜조회"])
    #         return True
    #     else:
    #         return False
    
    def isCurrency(self, text, syscategory):
        pattern = "{화폐단위}+{숫자}"
        if re.search('(달러|위안|유로|엔|파운드|루피|프랑|홍콩달러|싱가포르달러)', text) and re.search('\d', text):
            self.result.append([text, syscategory, "정치/경제/금융 > 환율/시세 > 환율", pattern])
            return True
        else:
            return False

    def PrintResult(self, syscategory):
        for i in range(len(self.result)):
            print("키워드: " + self.result[i][0] + '\n'
                  + "시스템카테고리: " + syscategory[i] + '\n'
                  + "카테고리: " + self.result[i][2] + '\n'
                  + "적용패턴: " + self.result[i][3] + '\n')



    def exportCsv(self):
        df = pd.DataFrame(self.result)
        df.columns = ["키워드", "시스템카테고리", "패턴카테고리", "적용패턴"]
        df.to_csv('comparative.txt', sep='\t', encoding='utf-8')












