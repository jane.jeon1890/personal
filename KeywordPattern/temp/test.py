
import re

testset = ["cgv영화관", "2019cgv채널", "cgv채널편성표", "cgv채널", "cgv개봉영화", "cgv편성표"]

i = 0

while i < len(testset):
    text = testset[i]

    if re.search('(cgv|CGV|메가박스|롯데시네마)', text) and re.search('^((?!채널).)*$', text) and re.search('^((?!편성표).)*$', text):
        print(text)

    i = i + 1

