import re

class Tool_box:

    def __init__(self):

        self.result = []

    def isSocialInsurance(self,text):
        if re.search('(4대보험|산재보험|국민연금|고용보험)', text):
            self.result.append([text, "정치/경제/금융 > 법률/제도 > 금융/복지제도"])
            return True
        else:
            return False

    def isHomePage(self, text):
        if re.search('(건강보험|4대보험|산재보험|국민연금|고용보험)', text) and re.search('(홈페이지|사이트|발급|인터넷발급|확인서|증명서)', text):
            self.result.append([text, "정치/경제/금융 > 커뮤니티/사이트"])
            return True
        else:
            return False

    def isInsuranceCal(self, text):
        if re.search('(보험)', text) and re.search('(계산기|계산)', text):
            self.result.append([text, "도구 > 계산기"])
            return True
        else:
            return False

    def isLaw(self, text):
        if re.search('(보험)', text) and re.search('(법$)', text):
            self.result.append([text, "정치/경제/금융 > 법률/제도 > 법률/제도정보"])
            return True
        else:
            return False

    def PrintResult(self):
        for i in range(len(self.result)):
            print("키워드: " + self.result[i][0] + '\n' + "카테고리: " + self.result[i][1] + '\n')












