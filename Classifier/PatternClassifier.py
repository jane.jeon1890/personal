class PatternClassifier:

    def __init__(self):

        from ReadCsv import ReadCsv
        ReadCsv.__init__(self)

        from Toolbox import Toolbox
        self.tool = Toolbox()

    def loop(self):
        inputData = self.keyword
        i = 0

        while i < len(inputData) - 1:
            self.tool.isTvProgram(inputData[i])
            self.tool.isMoviePremiere(inputData[i])
            self.tool.isTheater(inputData[i])
            self.tool.isBusNumber(inputData[i])
            self.tool.isBusStopNumber(inputData[i])
            self.tool.isStockSymbol(inputData[i])
            self.tool.isShippingNumber(inputData[i])
            self.tool.isPhoneNumber(inputData[i])
            self.tool.isAreaCode(inputData[i])
            self.tool.isBus(inputData[i])
            self.tool.isSubwayLine(inputData[i])
            self.tool.isTVGuide(inputData[i])
            self.tool.isCellPhone(inputData[i])
            self.tool.isDate(inputData[i])
            self.tool.isCurrency(inputData[i])
            i = i + 1

        self.tool.PrintResult()



