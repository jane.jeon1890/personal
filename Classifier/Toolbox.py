import re

class Toolbox:

    def __init__(self):

        self.result = []

        self.programtitle = input("프로그램 제목을 입력하세요: ")
        self.tvgenere = input("반복회차 프로그램 장르를 입력하세요: ")

        self.tvguidegenere = input("TV 편성표 장르를 입력하세요: ")


    def isTvProgram(self, text):
        if re.search(self.programtitle, text) and re.search('\d(회|화|차)', text):
            self.result.append([text, "방송 > 프로그램/채널 > " + self.tvgenere])
            return True
        else:
            return False

    def isMoviePremiere(self,text):
        if re.search('(개봉|개봉예정|상영일)', text):
            self.result.append([text, "영화 > 영화정보"])
            return True
        else:
            return False

    def isTheater(self, text):
        if re.search('(cgv|CGV|메가박스|롯데시네마)', text):
            self.result.append([text, "영화 > 영화관"])
            return True
        else:
            return False

    def isBusNumber(self, text):
        if re.compile(r'\d{1,4}$').match(text):
            self.result.append([text, "지도/지역/교통 > 교통수단 > 버스"])
            return True
        else:
            return False

    def isBusStopNumber(self, text):
        if re.compile(r'\d{5,5}$').match(text):
            self.result.append([text, "지도/지역/교통 > 도로 > 도로/노선"])
            return True
        else:
            return False

    def isStockSymbol(self, text):
        if re.compile(r'\d{6,6}$').match(text):
            self.result.append([text, "정치/경제/금융 > 증권/기업 > 주식시세정보"])
            return True
        else:
            return False

    def isShippingNumber(self, text):
        if re.compile(r'\d{10,13}$').match(text):
            self.result.append([text, "도구 > 조회기 > 택배조회"])
            return True
        else:
            return False

    def isPhoneNumber(self, text):
        if re.search('(\d{3}-\d{4}-\d{4}|\d{4}-\d{4})', text):
            self.result.append([text, "도구 > 조회기 > 기타정보"])
            return True
        else:
            return False

    def isAreaCode(self, text):
        if re.search('지역번호', text) or re.search('지역', text) and re.search('\d', text):
            self.result.append([text, "도구 > 조회기 > 지역정보"])
            return True
        else:
            return False

    def isBus(self, text):
        if re.search('버스', text) and re.search('\d', text):
            self.result.append([text, "지도/지역/교통 > 교통수단 > 버스"])
            return True
        else:
            return False

    def isSubwayLine(self, text):
        if re.search('\d(호선)', text):
            self.result.append([text, "지도/지역/교통 > 교통수단 > 지하철"])
            return True
        else:
            return False

    def isTVGuide(self, text):
        if re.search(self.tvguidegenere, text) and re.search('편성표', text):
            self.result.append([text, "방송 > 프로그램/채널 >" + self.tvguidegenere])
            return True
        else:
            return False

    def isCellPhone(self, text):
        if re.search('(아이폰|아이패드)\w', text):
            self.result.append([text, "쇼핑 > 디지털/가전 > 휴대폰"])
            return True
        else:
            return False

    def isDate(self, text):
        if re.search('\d(년|월|일)', text):
            self.result.append([text, "도구 > 조회기 > 날짜조회"])
            return True
        else:
            return False

    def isCurrency(self, text):
        if re.search('(달러|위안|유로|엔|파운드|루피|프랑|홍콩달러|싱가포르달러)', text) and re.search('\d', text):
            self.result.append([text, "정치/경제/금융 > 환율/시세 > 환율"])
            return True
        else:
            return False

    def PrintResult(self):
        for i in range(len(self.result)):
            print("키워드: " + self.result[i][0] + '\n' + "카테고리: " + self.result[i][1] + '\n')












