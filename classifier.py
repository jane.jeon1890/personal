import re

testset = ["무해지보험", "무해지암보험", "스카이캐슬1화", "스카이캐슬2화", "스카이캐슬3화", "스카이캐슬4화",
           "cgv강남", "cgv청담", "cgv압구정", "메가박스코엑스", "롯데월드타워롯데시네마", 
           "12345678", "123버스", "123456", "12345", 
           "영화개봉일", "호두까기인형공연", 
           "아이폰xs", "아이패드xs가격",
           "031지역", "055지역번호", "1588-0000",
           "2019년", "2019년1월1일", "31일",
           "1호선열차", "1호선사고", "1호선연장",
           "스카이캐슬드라마편성표", "아이폰홈쇼핑편성표", "12월개봉영화", "마약왕개봉일",
           ]

i = 0
programtitle = input("프로그램 제목을 입력하세요: ")
tvgenere = input("반복회차 프로그램 장르를 입력하세요: ")

while i < len(testset):
    i = i + 1
    text = testset[i]

    # 방송 프로그램 제목 + 회차정보 : {프로그램제목 string} + {number} + "회"
    if re.search(programtitle, text) and re.search('\d(회|화|차)', text):
        ans = "프로그램채널정보"
        print(text + f"는 {ans}일 확률이 높다. \n" + f"방송 > 프로그램/채널 > {tvgenere}" )
    
    # 개봉 : "개봉" + {string}
    elif re.search('(개봉|개봉예정|상영일)', text):
        ans = "영화정보"
        print(text + f"는 {ans}일 확률이 높다. \n" + "영화 > 영화정보" )

    # 영화관 : cgv, 메가박스, 롯데시네마
    elif re.search('(cgv|CGV|메가박스|롯데시네마)', text):
        ans = "영화관정보"
        print(text + f"는 {ans}일 확률이 높다. \n" + "영화 > 영화관" )
    
    # 4자리 이하 : 버스번호
    elif re.compile(r'\d{1,4}$').match(text):
        ans = "버스정보"
        print(text + f"는 {ans}일 확률이 높다. \n" + "지도/지역/교통 > 교통수단 > 버스")
    
    # 5자리 숫자 : 정거장 번호
    elif re.compile(r'\d{5,5}$').match(text):
        ans = "정거장번호"
        print(text + f"는 {ans}일 확률이 높다. \n" + "지도/지역/교통 > 도로 > 도로/노선")
    
    # 6자리 숫자 : 주식시세정보(종목코드)
    elif re.compile(r'\d{6,6}$').match(text):
        ans = "주식종목코드"
        print(text + f"는 {ans}일 확률이 높다. \n" + "정치/경제/금융 > 증권/기업 > 주식시세정보")

    # 10자리 이상, 13자리 이하 키워드 : 택배송장번호
    elif re.compile(r'\d{10,13}$').match(text):
        ans = "택배송장번호"
        print(text + f"는 {ans}일 확률이 높다. \n" + "도구 > 조회기 > 택배조회")

    # 4자리-4자리 전화번호
    elif re.compile(r'(\d{4})[-](\d{4})$').match(text):
        ans = "상업용전화번호"
        print(text + f"는 {ans}일 확률이 높다. \n" + "도구 > 조회기 > 기타정보")

    # 숫자 + 문자 : {number} + {지역번호, 지역}
    elif re.search('지역번호', text) or re.search('지역', text) and re.search('\d', text):
        ans = "지역번호"
        print(text + f"는 {ans}일 확률이 높다.\n" + "카테고리분류 : 도구 > 조회기 > 지역정보")
    
    # 숫자 + 버스 : {number} + {버스}
    elif re.search('버스', text) and re.search('\d', text):
        ans = "버스번호"
        print(text + f"는 {ans}일 확률이 높다.\n" + "지도/지역/교통 > 교통수단 > 버스")
    
    # 숫자 + 호선 : {number, string} + {호선}
    elif re.search('\d(호선)', text):
        ans = "지하철정보"
        print(text + f"는 {ans}일 확률이 높다. \n" + "지도/지역/교통 > 교통수단 > 지하철" )
    
    # 편성표 : {편성표} + {드라마, 스포츠, 예능, 홈쇼핑}
    elif re.search('(드라마|스포츠|예능)', text) and re.search('편성표', text):
        ans = "편성표정보"
        genere = re.search('(드라마|스포츠|예능)', text).group(1)
        print(text + f"는 {ans}일 확률이 높다. \n" + f"방송 > 프로그램/채널 > {genere}" )
    
    # 아이패드, 아이폰 : {아이패드, 아이폰} + {모델명}
    elif re.search('(아이폰|아이패드)\w', text):
        ans = "휴대폰정보" 
        print(text + f"는 {ans}일 확률이 높다. \n" + "쇼핑 > 디지털/가전 > 휴대폰" )
    
    # 숫자 + 연/월/일 : {number} + {년, 월, 일}
    elif re.search('\d(년|월|일)', text):
        ans = "날짜정보" 
        print(text + f"는 {ans}일 확률이 높다. \n" + "도구 > 조회기 > 날짜조회" )

    # 숫자 + 화폐단위 : {number} + {위안화, 달러, 페소, 유로, ...}
    elif re.search('(달러|위안|유로|엔|파운드|루피|프랑|홍콩달러|싱가포르달러)', text) and re.search('\d', text):
        ans = "화폐단위"
        print(text + f"는 {ans}일 확률이 높다. \n" + "정치/경제/금융 > 환율/시세 > 환율" )
    
    