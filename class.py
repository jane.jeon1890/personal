
#%%
testset = ["무해지보험", "무해지암보험", "스카이캐슬1화", "스카이캐슬2화", "스카이캐슬3화", "스카이캐슬4화", 
           "cgv강남", "cgv청담", "cgv압구정", "메가박스코엑스", "롯데월드타워롯데시네마", 
           "12345678", "123버스", "123456", "12345", 
           "영화개봉일", "호두까기인형공연", 
           "아이폰xs", "아이패드xs가격"]


#%%
import re
title = "스카이캐슬"
category = "드라마"
text = "스카이캐슬1회"
test = True if re.search(title, text) and re.search('\d(회|화|차)', text) else print("False")
ans = "프로그램채널정보" if test == True else print("False")

print(text + f"는 {ans}일 확률이 높다. \n" + f"방송 > 프로그램/채널 > {category}" ) if ans != None else print("None")


#%%
title = "영화개봉일"
category = "드라마"
text = "스카이캐슬1회"
test = True if re.search(title, text) and re.search('\d(회|화|차)', text) else print("False")
ans = "프로그램채널정보" if test == True else print("False")
print(text + f"는 {ans}일 확률이 높다. \n" + f"방송 > 프로그램/채널 > {category}" ) if ans != None else print("None")


#%%
for i in range(len(testset)):
    print(testset[i])
    


#%%
def classifier_func():
    for i in range(len(testset)):
        text = testset[i]
        category = "드라마"
        test = True if re.search(title, text) and re.search('\d(회|화|차)', text) else print("False")
        ans = "프로그램채널정보" if test == True else print("False")
        print(text + f"는 {ans}일 확률이 높다. \n" + f"방송 > 프로그램/채널 > {category}" ) if ans != None else print("실패")


#%%
classifier_func()


#%%
testset = ["무해지보험", "무해지암보험", "스카이캐슬1화", "스카이캐슬2화", "스카이캐슬3화", "스카이캐슬4화", 
           "cgv강남", "cgv청담", "cgv압구정", "메가박스코엑스", "롯데월드타워롯데시네마", 
           "12345678", "123버스", "123456", "12345", 
           "영화개봉일", "호두까기인형공연", 
           "아이폰xs", "아이패드xs가격"]

class classifier:     
    def filter(self):
        for i in range(len(testset)):
            text = testset[i]
            category = "드라마"
            test = True if re.search(title, text) and re.search('\d(회|화|차)', text) else print("False")
            ans = "프로그램채널정보" if test == True else print("False")
            print(text + f"는 {ans}일 확률이 높다. \n" + f"방송 > 프로그램/채널 > {category}" ) if ans != None else print("실패")
            

#%%
def switch_func(value, x):
    return {
        'a': lambda x: x+122,
        'b': lambda x: x*2,
        'c': lambda x: x-123,
        'd': lambda x: x/2
    }.get(value)(x)

# take user input
inp = input('input a character : ')
print('The result for inp is : ', switch_func(inp, 2))

#%%
class Example(object):
    #This is what we use to call every function
    def functionCaller(self, functionName):
        #This is the function that needs to 
        #be called every time
        self.FunctionThatGetsCalledFirst()
        #Now we call the function that was supplied
        exec("self."+functionName+"()")
    def FunctionThatGetsCalledFirst(self):
	print("First")
	
    def Test(self):
        print("Hello")
        
e = Example()
#Try and call the function Test()
e.functionCaller("Test")
"""
First
Hello
"""


